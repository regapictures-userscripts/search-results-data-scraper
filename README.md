# Search results data scraper

## Functions
- Copy search results to clipboard

## Install
https://regapictures-userscripts.gitlab.io/search-results-data-scraper/bundle.user.js

## Download
https://gitlab.com/regapictures-userscripts/search-results-data-scraper/-/jobs/artifacts/master/raw/dist/bundle.user.js?job=build