export type CardFilter = (title: string | undefined, values: string[]) => boolean;
export type EvenOddOrBoth = 'both' | 'even' | 'odd';

export interface FilterOptions {
    evenOrOdd: EvenOddOrBoth;
    minimum: number | undefined;
    maximum: number | undefined;
};