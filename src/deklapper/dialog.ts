import { FilterOptions } from "./filter-options";

export class Dialog {
    private readonly copyOptionsEvenOrOddName = 'evenOrOdd';
    private width: number;
    private height: number;
    private element: HTMLDivElement;

    constructor() {
        this.width = 500;
        this.height = 250;
        this.element = document.createElement('div');
        this.element.style.boxSizing = 'border-box';
        this.element.style.width = this.width + 'px';
        this.element.style.height = this.height + 'px';
        this.element.style.position = 'fixed';
        this.element.style.top = '50%';
        this.element.style.left = '50%';
        this.element.style.zIndex = '10';
        this.element.style.marginTop = `-${this.height / 2}px`;
        this.element.style.marginLeft = `-${this.width / 2}px`;
        this.element.style.backgroundColor = 'white';
        this.element.style.border = '1px solid gray';
        this.element.style.borderRadius = '5px';
        this.element.style.boxShadow = '0px 0px 10px #444';
        this.element.style.padding = '10px 20px';
    }

    public close() {
        // Detaches the element, but keeps it in memory
        const parent = this.element.parentElement;
        if (parent != undefined) {
            this.element.innerHTML = '';
            parent.removeChild(this.element);
        }
    }

    public openWithCopyOptions(onCopyWithoutFilter: () => void, onCopyWithFilter: (filters: FilterOptions) => void) {
        this.element.innerHTML = '<div id="copy-options-header" style="font-size: 1.3em; font-weight: bold; display: flex">' +
            '<div style="flex-grow: 1">Wenst u op huisnummer te filteren?</div>' +
            '</div>' +
            '<div style="font-size: 1.1em; font-weight: bold; margin-top: 10px">Even of oneven?</div>' +
            '<label for="evenOrOdd-both" style="display: block">' +
            '<input type="radio" style="margin-right: 10px" name="' + this.copyOptionsEvenOrOddName + '" id="evenOrOdd-both" value="both" checked="checked" />Behoud beide</label>' +
            '<label for="evenOrOdd-even" style="display: block">' +
            '<input type="radio" style="margin-right: 10px" name="' + this.copyOptionsEvenOrOddName + '" id="evenOrOdd-even" value="even" />Behoud alleen even</label>' +
            '<label for="evenOrOdd-odd" style="display: block">' +
            '<input type="radio" style="margin-right: 10px" name="' + this.copyOptionsEvenOrOddName + '" id="evenOrOdd-odd" value="odd" />Behoud alleen oneven</label>' +
            '<div style="font-size: 1.1em; font-weight: bold; margin-top: 10px">Bereik beperken?</div>' +
            '<div id="range-min-polyfill"></div>' +
            '<div id="range-max-polyfill"></div>' +
            '<div style="display: flex; margin-top: 10px" id="copy-options-buttons"><div style="flex-grow: 1"></div>' +
            '<div style="margin-right: 5px; font-weight: bold">Kopieer:</div>';

        document.body.appendChild(this.element);

        const rangeMin = this.createCheckableNumericInput('Minimum', 'range-min');
        const rangeMax = this.createCheckableNumericInput('Maximum', 'range-max');
        rangeMax.style.marginTop = '2px';
        this.element.replaceChild(rangeMin, <HTMLElement>document.getElementById('range-min-polyfill'));
        this.element.replaceChild(rangeMax, <HTMLElement>document.getElementById('range-max-polyfill'));

        const closeCross = document.createElement('div');
        closeCross.innerText = '✕';
        closeCross.style.cursor = 'pointer';
        closeCross.addEventListener('click', () => this.close());
        const header = <HTMLDivElement>document.getElementById('copy-options-header');
        header.appendChild(closeCross);
        const dontFilterButton = document.createElement('button');
        dontFilterButton.innerText = 'Zonder filteren';
        dontFilterButton.addEventListener('click', () => {
            this.close();
            onCopyWithoutFilter();
        });
        const filterButton = document.createElement('button');
        filterButton.innerText = 'Met filteren';
        filterButton.addEventListener('click', () => {
            const filters = this.getSelectedFilters();
            this.close();
            onCopyWithFilter(filters);
        });
        const buttonWrapper = <HTMLDivElement>document.getElementById('copy-options-buttons');
        buttonWrapper.appendChild(dontFilterButton);
        buttonWrapper.appendChild(filterButton);
    }

    private createCheckableNumericInput(description: string, baseId: string) {
        const checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        const checkboxId = baseId + '-enabled';
        checkbox.name = checkboxId;
        checkbox.id = checkboxId;
        checkbox.style.marginRight = '10px';
        const text = document.createElement('span');
        text.innerText = description;
        const numericInput = document.createElement('input');
        numericInput.type = 'number';
        numericInput.min = '1';
        numericInput.style.marginLeft = '10px';
        numericInput.style.padding = '0px';
        numericInput.style.width = '60px';
        numericInput.name = baseId + '-value'
        numericInput.id = baseId + '-value'
        numericInput.addEventListener('change', () => checkbox.checked = numericInput.value.trim() !== '');
        const label = document.createElement('label');
        label.style.display = 'block';
        label.htmlFor = checkboxId;
        label.appendChild(checkbox);
        label.appendChild(text);
        label.appendChild(numericInput);
        return label;
    }

    public openCopiedConfirmation(closeTimeMs: number) {
        const msgElement = document.createElement('div');
        msgElement.style.fontSize = '1.4em';
        msgElement.style.position = 'absolute';
        msgElement.style.top = 'calc(50% - 1em)';
        msgElement.style.left = 'calc(50% - 65px)';
        msgElement.style.width = '130px';
        msgElement.style.lineHeight = '2em';
        msgElement.style.textAlign = 'center';
        msgElement.innerText = 'Gekopieerd!';
        this.element.appendChild(msgElement);
        document.body.appendChild(this.element);
        setTimeout(() => this.close(), closeTimeMs);
    }

    public openCopyFailed(closeTimeMs: number) {
        const msgElement = document.createElement('div');
        msgElement.style.fontSize = '1.4em';
        msgElement.style.position = 'absolute';
        msgElement.style.top = 'calc(50% - 1em)';
        msgElement.style.left = 'calc(50% - 65px)';
        msgElement.style.width = '130px';
        msgElement.style.lineHeight = '2em';
        msgElement.style.textAlign = 'center';
        msgElement.style.color = '#a00';
        msgElement.innerText = 'Mislukt!';
        this.element.appendChild(msgElement);
        document.body.appendChild(this.element);
        setTimeout(() => this.close(), closeTimeMs);
    }

    private getSelectedFilters(): FilterOptions {
        const checkedEvenOrOddRadioButton = <HTMLInputElement | undefined>document.querySelector('input[name="' + this.copyOptionsEvenOrOddName + '"]:checked');
        const evenOddOrBoth = <'both' | 'even' | 'odd'>(checkedEvenOrOddRadioButton?.value ?? 'both');

        return {
            evenOrOdd: evenOddOrBoth,
            minimum: this.getRangeValue('range-min-enabled', 'range-min-value'),
            maximum: this.getRangeValue('range-max-enabled', 'range-max-value'),
        }
    }

    private getRangeValue(checkboxId: string, numberInputName: string) {
        const checkboxEnabled = (<HTMLInputElement>document.getElementById(checkboxId)).checked;
        const numberInputValue = Number((<HTMLInputElement>document.getElementsByName(numberInputName)[0]).value);
        return !checkboxEnabled || isNaN(numberInputValue) || numberInputValue < 1 ? undefined : numberInputValue;
    }
}