import { Waiter } from "../common";
import { Dialog } from "./dialog";
import { CardFilter, FilterOptions } from "./filter-options";

export class DeKlapperScraper {
    private readonly copyButtonId = 'copy-results';
    private readonly copyButtonText = 'Kopieer huidige pagina';
    private waiter = new Waiter(60000);
    private dialog = new Dialog();

    public run() {
        const searchButton = this.findSearchButton();
        if (searchButton != undefined) {
            searchButton.addEventListener('click', () => this.afterSearchClick());
        }
    }

    private afterSearchClick() {
        this.waiter.await(
            () => this.findSearchResultsHeader() != undefined,
            () => this.addCopyResultsButton()
        );
    }

    private addCopyResultsButton() {
        if (this.findCopyResultsButton() != undefined) {
            return;
        }

        const container = this.findSearchResultsPaginationContainer();
        if (container != undefined) {
            const button = document.createElement('button');
            button.id = this.copyButtonId;
            button.innerText = this.copyButtonText;
            button.style.marginRight = '10px';
            button.style.top = '0.3333em'; // Element is positioned relative by the page
            button.addEventListener('click', () => this.openOptions(button));
            container.prepend(button);
        }
    }

    private openOptions(copyButton: HTMLButtonElement) {
        if (this.dialog == undefined) {
            this.dialog = new Dialog();
        }

        this.dialog.openWithCopyOptions(
            () => this.copyWithoutFilter(copyButton),
            filters => this.copyWithFilter(copyButton, filters)
        );
    }

    private copyWithoutFilter(copyButton: HTMLButtonElement) {
        this.copyResults(copyButton, () => true);
    }

    private copyWithFilter(copyButton: HTMLButtonElement, selectedFilters: FilterOptions) {
        this.copyResults(copyButton, (title, values) => {
            // Only filter out when we're sure it is NOT a match, otherwise don't filter (return true)
            if (values.length === 0) return true;
            
            const matches = (/\w+\s(\d+)/g).exec(values[0]);
            if (matches == undefined || matches.length !== 2) return true;

            const houseNumber = Number(matches[1]);
            const isEven = (houseNumber % 2 === 0);
            return (selectedFilters.evenOrOdd === 'both' || (selectedFilters.evenOrOdd === 'even' && isEven) || (selectedFilters.evenOrOdd === 'odd' && !isEven))
                && (selectedFilters.minimum == undefined || selectedFilters.minimum <= houseNumber)
                && (selectedFilters.maximum == undefined || selectedFilters.maximum >= houseNumber);
        });
    }

    private copyResults(copyButton: HTMLButtonElement, filter: CardFilter) {
        copyButton.disabled = true;
        copyButton.style.opacity = '0.3';
        try {
            GM_setClipboard(this.scrapeResults(filter), 'text');
            copyButton.innerText = 'Gekopieerd';
            this.dialog.openCopiedConfirmation(2500);
        } catch (err) {
            console.error('Failed to scrape results', err);
            copyButton.innerText = 'Mislukt';
            this.dialog.openCopyFailed(2500);
        }
        setTimeout(() => {
            copyButton.innerText = this.copyButtonText;
            copyButton.disabled = false;
            copyButton.style.opacity = '1';
        }, 2500);
    }

    private scrapeResults(filter: CardFilter): string {
        const resultsContainer = this.findSearchResultsContainer();
        if (resultsContainer == undefined) {
            throw new Error('Missing search results container');
        }

        const cardValues = <string[]>Array.from(resultsContainer.querySelectorAll('div.infobelpro_card'))
            .map(card => this.scrapeCard(<HTMLDivElement>card, filter))
            .filter(content => content != undefined);

        if (cardValues.length > 0) {
            return cardValues.reduce((v1, v2) => v1 + '\n\n' + v2);
        } else {
            throw new Error('Geen zoekresultaten gedetecteerd');
        }
    }

    private scrapeCard(card: HTMLDivElement, filter: CardFilter): string | undefined {
        const titleElement = (<HTMLSpanElement>card.querySelector('span.infobelpro_title'));
        const values = Array.from(<NodeListOf<HTMLSpanElement>>card.querySelectorAll('span.infobelpro_postfix'))
            .map(valueElement => valueElement.innerText.trim())
            .filter(value => value !== '');

        let title: string | undefined = undefined;
        if (titleElement != undefined) {
            const titleText = titleElement.innerText.trim();
            if (titleText !== '') {
                title = titleText;
            }
        }

        if (filter(title, values)) {
            const data = values.length > 0 ? values.reduce((v1, v2) => v1 + '\n' + v2) : '<geen informatie>';
            return (title ?? '<geen naam>') + '\n' + data;
        } else {
            return undefined;
        }
    }

    private findSearchButton() {
        return <HTMLButtonElement | undefined>document.querySelector('#block-infobelpro-searchblock #infobelpro_tabs div#infobelpro_tabContainer > div:last-child > button');
    }

    private findSearchResultsHeader() {
        return <HTMLDivElement | undefined>document.querySelector('#infobelpro_result_person > div.infobelpro_count');
    }

    private findSearchResultsPaginationContainer() {
        return <HTMLDivElement | undefined>document.querySelector('#infobelpro_result_person > div.infobelpro_count > div.infobelpro_pagination');
    }

    private findSearchResultsContainer() {
        return <HTMLDivElement | undefined>document.querySelector('#infobelpro_result_person > div#infobelpro_resultCards_person');
    }

    private findCopyResultsButton() {
        return <HTMLButtonElement | undefined>document.querySelector('button#' + this.copyButtonId);
    }
}
