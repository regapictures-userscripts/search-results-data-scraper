export class Waiter {
    private timer: NodeJS.Timeout | undefined;

    constructor(private timeoutMs: number) { }

    public await(condition: () => boolean, whenTrueHandler: () => void) {
        this.cancelWait();
        this.tick(condition, whenTrueHandler);
    }

    public cancelWait() {
        if (this.timer != undefined) {
            clearTimeout(this.timer);
            this.timer = undefined;
        }
    }

    private tick(condition: () => boolean, whenTrueHandler: () => void, totalAttemptTimeMs: number = 0) {
        if (condition()) {
            this.timer = undefined;
            whenTrueHandler();
        } else if (totalAttemptTimeMs <= this.timeoutMs) {
            this.timer = setTimeout(() => this.tick(condition, whenTrueHandler, totalAttemptTimeMs + 500), 500);
        }
    }
}